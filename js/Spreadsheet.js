/*
*	Eliminar este comentario al copiar al appscript de tu spreadsheet:
*	Crear un spreadsheet y crear su appscript interno.
*	Habilitar el activador que active la funcion onEdit() cada vez que se edite.
*/
function onEdit(e){
  var spread = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = spread.getSheets()[0];
  
  var rang = e.range;
  var fila = rang.getRow();
  var fecha = (new Date()).getTime();// Se obtiene la fecha actual, pero convertido en milisegundos desde 1970
  
  sheet.setColumnWidth(1, 1); // Define (columna, ancho en pixeles)
  sheet.getRange(fila, 1).setValue(fecha);
  sheet.getRange(1, 1).setValue('ACTUALIZA');
  sheet.getRange(1, 2).setValue('FECHA');
  sheet.getRange(1, 3).setValue('EVENTO');
  sheet.getRange(1, 4).setValue('URGENTE');

  SpreadsheetApp.flush(); // Esto actualiza el spreadsheet luego de ejecutar el script
}
