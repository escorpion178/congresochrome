
var tooltipContent=0;

$(document).ready(function() {
    $.ajaxSetup({ cache: false });
    var iconCongreso = chrome.extension.getURL('../img/congreso.png');
    var iconAgenda = chrome.extension.getURL('../img/agenda.png');
    var iconCalendar = chrome.extension.getURL('../img/calendar.png');
    var iconTextEdit = chrome.extension.getURL('../img/text-edit.png');
    var iconTours = chrome.extension.getURL('../img/tours.png');
    $(".MainMenuMI").prepend('<div id="tool" class="tooltip">'+
            '<img id="MiCongreso" src="'+iconCongreso+'"/> '+
            '<span class="tooltip-content">'+
                '<span class="tooltip-text">'+
                    '<span class="tooltip-inner">'+
                        '<div class="caja">'+
                            '<ul class="itemas" style="width: 470px;">'+
                                '<li class="itema">'+
                                    '<a class="linkli" href="http://www.congreso.gob.pe/sintesisacuerdoscomisiones" target="_blank">'+
                                        '<img class="icoco" src="'+iconTextEdit+'">'+
                                            '<span class="txt">Acuerdos</span></a></li>'+
                                '<li class="itema">'+
                                    '<a class="linkli" href="http://www.congreso.gob.pe/sintesisagendascomisiones" target="_blank">'+
                                        '<img class="icoco" src="'+iconAgenda+'">'+
                                        '<span class="txt">Agendas</span></a></li>'+
                                '<li class="itema">'+
                                    '<a class="linkli" href="http://www.congreso.gob.pe/sintesisagendascomisiones" target="_blank">'+
                                        '<img class="icoco" src="'+iconCalendar+'">'+
                                        '<span class="txt">Calendar</span></a></li>'+
                                '<li class="itema">'+
                                    '<a class="linkli" onclick="" href="#">'+
                                        '<img class="icoco" src="'+iconTours+'">'+
                                        '<span class="txt">Congresistas</span></a></li>'+
                            '</ul>'+
                        '</div>'+
                    '</span>'+
                '</span>'+
            '</span>'+
        '</div>'
    );
    $("#MiCongreso").click(function(obj){
        if(tooltipContent==0){            
            $("#tool").addClass("tooltip-click");
            tooltipContent=1;
        }else{
            $("#tool").removeClass("tooltip-click");
            tooltipContent=0;
        }       
    });
    
});

